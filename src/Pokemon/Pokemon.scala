package Pokemon

import Types.Type
import math.max

abstract class Pokemon(val SpeciesStats: Stats) {
  val types: List[Type]
  val currentState: State = new State(SpeciesStats)


  def receiveAttack(power: Int): Int = {
    val damage: Int = calculateDamage(power)
    currentState.receiveDamage(damage)
    return damage
  }

  def calculateDamage(power: Int): Int =
    2 * max(3, power - this.currentState.stats.Defense)

  def attack(other: Pokemon): Int =
    other.receiveAttack(this.currentState.stats.Attack + 3)

}
