package Pokemon

class State(var initialStats: Stats) {
  var Health: Int = 100
  var Mana: Int = 100

  var stats: Stats = initialStats

  def receiveDamage(damage: Int): Int = {
    Health -= damage
    return damage
  }
}
