package Pokemon.Species

import Pokemon.{Pokemon, Stats}
import Types.{Grass, Poison}

class Bulbasaur extends Pokemon(Stats(45, 49, 49, 65, 65, 45)){
  val types = List(Grass, Poison)
}
