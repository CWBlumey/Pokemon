package Pokemon.Species

import Pokemon.{Pokemon, Stats}
import Types.Water

class Squirtle extends Pokemon(Stats(44, 48, 65, 50, 64, 43)) {
  val types = List(Water)
}
