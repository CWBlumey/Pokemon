package Pokemon.Species

import Pokemon.{Pokemon, Stats}
import Types.Fire

class Charmander extends Pokemon(Stats(39, 52, 43, 60, 50, 65)) {
  val types = List(Fire)
}
