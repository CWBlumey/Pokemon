package Pokemon

case class Stats(HP: Int,
                 Attack: Int,
                 Defense: Int,
                 SpecialAttack: Int,
                 SpecialDefense: Int,
                 Speed: Int)
{
}
