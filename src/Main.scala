import Pokemon.Species.{Charmander, Squirtle}

object Main extends App {
  def main(): Unit = {
    val squirtle = new Squirtle()
    val charmander = new Charmander()

    charmander.attack(squirtle)
    print(squirtle.currentState.Health)
  }
  main()
}
