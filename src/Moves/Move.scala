package Moves

import Types.Type

trait Move {
  val moveType: Type
}