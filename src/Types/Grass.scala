package Types

object Grass extends Type {
  override val weaknesses: List[Type] = List(Fire, Poison)
  override val resistances: List[Type] = List(Water, Grass)
}
