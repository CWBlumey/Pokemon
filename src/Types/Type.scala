package Types

trait Type {
  val weaknesses: List[Type]
  val resistances: List[Type]
}
