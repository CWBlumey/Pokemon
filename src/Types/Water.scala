package Types

object Water extends Type{
  override val weaknesses: List[Type] = List(Grass)
  override val resistances: List[Type] = List(Fire, Water)
}
