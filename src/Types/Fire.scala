package Types

object Fire extends Type{
  val weaknesses: List[Type] = List(Water)
  val resistances: List[Type] = List(Grass, Fire)
}
