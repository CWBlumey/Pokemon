package Types

object Poison extends Type {
  override val weaknesses: List[Type] = List()
  override val resistances: List[Type] = List(Grass)
}
